package org.academiadecodigo.loopeytunes.Game;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background extends BackgroundPosition {

    private Picture background;

    public Background(){
        super(0 , 0);
    }

    public void startBackground() {
        Picture grid = new Picture(PADDING, PADDING , "MovableBackground.jpg");
        grid.draw();
    }

    public void startBackgroundLeft() {
        background = new Picture(PADDING, PADDING, "BackgrpundTwo.jpg");
        background.draw();
    }

    public void startBackgroundRigth() {
        background = new Picture(PADDING, PADDING, "BackgroundThree.jpg");
        background.draw();
    }

    public void startBackgroundUp() {
        background = new Picture(PADDING, PADDING, "BackgroundFour.jpg");
        background.draw();
    }

    public void startBackgroundDown() {
        background = new Picture(PADDING, PADDING, "BackgroundOne.jpg");
        background.draw();

    }

    public void startBoss1Room() {
        background = new Picture(PADDING, PADDING, "Background.jpg");
        background.draw();
    }

    public void endBossRoom() {
        background.delete();
    }

}
