package org.academiadecodigo.loopeytunes.Game;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Screen implements KeyboardHandler {

    Picture startScreen;
    private boolean start = false;

    public Screen(){
       this.startScreen = new Picture(10,10,"Menu.jpg");
       startScreen.draw();
    }

    public boolean start(){
        return start;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_ENTER) {
            startScreen.delete();
            start = true;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
