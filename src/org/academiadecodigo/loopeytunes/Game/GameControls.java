package org.academiadecodigo.loopeytunes.Game;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

public class GameControls {

    public void gameControls(Keyboard kb) {

        KeyboardEvent onePressed = new KeyboardEvent();
        onePressed.setKey(KeyboardEvent.KEY_1);
        onePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent twoPressed = new KeyboardEvent();
        twoPressed.setKey(KeyboardEvent.KEY_2);
        twoPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent threePressed = new KeyboardEvent();
        threePressed.setKey(KeyboardEvent.KEY_3);
        threePressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent fourPressed = new KeyboardEvent();
        fourPressed.setKey(KeyboardEvent.KEY_4);
        fourPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent wPressed = new KeyboardEvent();
        wPressed.setKey(KeyboardEvent.KEY_W);
        wPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent dPressed = new KeyboardEvent();
        dPressed.setKey(KeyboardEvent.KEY_D);
        dPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent aPressed = new KeyboardEvent();
        aPressed.setKey(KeyboardEvent.KEY_A);
        aPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent sPressed = new KeyboardEvent();
        sPressed.setKey(KeyboardEvent.KEY_S);
        sPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent enterPressed = new KeyboardEvent();
        enterPressed.setKey(KeyboardEvent.KEY_ENTER);
        enterPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        kb.addEventListener(enterPressed);
        kb.addEventListener(onePressed);
        kb.addEventListener(twoPressed);
        kb.addEventListener(threePressed);
        kb.addEventListener(fourPressed);
        kb.addEventListener(wPressed);
        kb.addEventListener(aPressed);
        kb.addEventListener(dPressed);
        kb.addEventListener(sPressed);

    }
}
