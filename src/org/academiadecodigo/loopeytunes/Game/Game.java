package org.academiadecodigo.loopeytunes.Game;

import org.academiadecodigo.loopeytunes.Character.*;
import org.academiadecodigo.loopeytunes.Sound;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;

public class Game {

    private boolean gameOver = false;
    private static Boss boss;
    GameControls gameControls = new GameControls();
    Screen startScreen = new Screen();
    Keyboard kb = new Keyboard(startScreen);


    public void start() throws InterruptedException {

        Sound mainMusic = new Sound("/mainMusic (online-audio-converter.com).wav");
        mainMusic.play(true);
        gameControls.gameControls(kb);
        startScreen.start();
        while (!startScreen.start()) {
            Thread.sleep(1);
        }

        Background background = new Background();
        MovablePlayer movablePlayer = new MovablePlayer(40, 30);
        Keyboard movablePlayerKb = new Keyboard(movablePlayer);
        background.startBackground();
        gameControls.gameControls(movablePlayerKb);


        while (true) {
            movablePlayer.setFightingStatusOff();

            Thread.sleep(1);
            if (movablePlayer.col <= 5) {
                movablePlayer.setFightingStatusOn();
                movablePlayer.deletePLayer();
                mainMusic.stop();
                checkRoomLeft();
                break;
            }
            if (movablePlayer.col >= BackgroundPosition.backgroundCols - 5) {
                movablePlayer.setFightingStatusOn();
                movablePlayer.deletePLayer();
                mainMusic.stop();
                checkRoomRigth();
                break;
            }
            if (movablePlayer.row <= 5) {
                movablePlayer.setFightingStatusOn();
                movablePlayer.deletePLayer();
                mainMusic.stop();
                checkRoomUp();
                break;
            }
            if (movablePlayer.row >= BackgroundPosition.backgroundRows - 10) {
                movablePlayer.setFightingStatusOn();
                movablePlayer.deletePLayer();
                mainMusic.stop();
                checkRoomDown();
                break;
            }
        }

    }

    public void checkRoomRigth() throws InterruptedException {

        //Background DRAW
        Background background = new Background();
        background.startBackgroundRigth();
        Sound bossMusic = new Sound("/sidBoss.wav");
        bossMusic.play(true);
        Thread.sleep(600);
        //PLayer + Boss draw
        Player player = new Player(100, 10, BackgroundPosition.cellSize, BackgroundPosition.cellSize);
        Keyboard playerKb = new Keyboard(player);
        boss = new Boss(100, 10, 120, 25);
        player.healthBar();
        boss.healthBar();
        gameControls.gameControls(playerKb);


        while (!gameOver) {

            Animation.initalStance(1);

            if (player.getHealth() <= 0 || boss.getHealth() <= 0) {

                background.endBossRoom();
                boss.deleteHealthBar();
                player.deleteHealthBar();
                gameOver = true;
                Animation.deleteAnimation();

                break;
            }

            player.chooseOption(boss, 1);
        }
        gameOver = false;
        boss.defeatedBossCounter();
        bossMusic.stop();
        start();
    }

    public void checkRoomLeft() throws InterruptedException {

        //Background DRAW
        Background background = new Background();
        background.startBackgroundLeft();
        Sound bossMusic = new Sound("/bossMusic.wav");
        bossMusic.play(true);
        Thread.sleep(600);
        //PLayer + Boss draw
        Player player = new Player(100, 10, BackgroundPosition.cellSize, BackgroundPosition.cellSize);
        Keyboard playerKb = new Keyboard(player);
        boss = new Boss(100, 10, 120, 25);
        player.healthBar();
        boss.healthBar();
        gameControls.gameControls(playerKb);


        while (!gameOver) {

            Animation.initalStance(3);

            if (player.getHealth() <= 0 || boss.getHealth() <= 0) {

                String endMessage = player.getHealth() > boss.getHealth() ? "You won" : "You lose";
                System.out.println(endMessage);
                background.endBossRoom();
                boss.deleteHealthBar();
                player.deleteHealthBar();
                Animation.deleteAnimation();
                gameOver = true;
            }

            player.chooseOption(boss, 3);
        }
        gameOver = false;
        boss.defeatedBossCounter();
        bossMusic.stop();
        start();
    }

    public void checkRoomUp() throws InterruptedException {

        //Background DRAW
        Background background = new Background();
        background.startBackgroundUp();
        Sound bossMusic = new Sound("/pedroBossMusic.wav");
        bossMusic.play(true);
        Thread.sleep(600);
        //PLayer + Boss draw
        Player player = new Player(100, 10, BackgroundPosition.cellSize, BackgroundPosition.cellSize);
        Keyboard playerKb = new Keyboard(player);
        boss = new Boss(100, 10, 120, 25);
        player.healthBar();
        boss.healthBar();
        gameControls.gameControls(playerKb);

        while (!gameOver) {

            Animation.initalStance(2);

            if (player.getHealth() <= 0 || boss.getHealth() <= 0) {

                background.endBossRoom();
                boss.deleteHealthBar();
                player.deleteHealthBar();
                Animation.deleteAnimation();
                gameOver = true;
            }

            player.chooseOption(boss, 2);
        }
        gameOver = false;
        boss.defeatedBossCounter();
        bossMusic.stop();
        start();
    }

    public void checkRoomDown() throws InterruptedException {

        //Background DRAW
        Background background = new Background();
        background.startBackgroundDown();
        Sound bossMusic = new Sound("/prisMusic.wav");
        bossMusic.play(true);
        Thread.sleep(600);
        //PLayer + Boss draw
        Player player = new Player(100, 10, BackgroundPosition.cellSize, BackgroundPosition.cellSize);
        Keyboard playerKb = new Keyboard(player);
        boss = new Boss(100, 10, 120, 25);
        player.healthBar();
        boss.healthBar();
        gameControls.gameControls(playerKb);

        while (!gameOver) {

            Animation.initalStance(4);

            if (player.getHealth() <= 0 || boss.getHealth() <= 0) {
                Thread.sleep(200);
                background.endBossRoom();
                boss.deleteHealthBar();
                player.deleteHealthBar();
                Animation.deleteAnimation();
                gameOver = true;
            }

            player.chooseOption(boss, 4);
        }
        gameOver = false;
        boss.defeatedBossCounter();
        bossMusic.stop();
        start();
    }
}


