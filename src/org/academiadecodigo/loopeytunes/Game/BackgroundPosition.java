package org.academiadecodigo.loopeytunes.Game;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class BackgroundPosition {

    protected final static int cellSize = 10;
    protected final static int cellSizeHeigth = 30;
    protected final static int PADDING = 10;
    protected int col;
    protected int row;
    protected final static int backgroundRows = 80;
    protected final static int backgroundCols = 160;

    public BackgroundPosition(int col , int row) {
        this.col = col;
        this.row = row;
        }
    }
