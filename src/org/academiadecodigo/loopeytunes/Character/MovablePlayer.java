package org.academiadecodigo.loopeytunes.Character;

import org.academiadecodigo.loopeytunes.Game.BackgroundPosition;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class MovablePlayer extends BackgroundPosition implements KeyboardHandler {

    Picture movablePlayer;
    boolean movablePlayerFighting;

    public MovablePlayer(int col, int row) {

        super(col, row);

        movablePlayer = new Picture(col * BackgroundPosition.cellSize, row * BackgroundPosition.cellSize, "MovablePlayerBack.png");
        movablePlayer.draw();
    }

    public void setMovablePlayerUp() {
        movablePlayer.delete();
        movablePlayer = new Picture(col * BackgroundPosition.cellSize, row * BackgroundPosition.cellSize, "MovablePlayerLeft.png");
        movablePlayer.draw();
    }

    public void setMovablePlayerDown() {
        movablePlayer.delete();
        movablePlayer = new Picture(col * BackgroundPosition.cellSize, row * BackgroundPosition.cellSize, "MovablePlayerRight.png");
        movablePlayer.draw();
    }

    public void setMovablePlayerLeft() {
        movablePlayer.delete();
        movablePlayer = new Picture(col * BackgroundPosition.cellSize, row * BackgroundPosition.cellSize, "MovablePlayerBack.png");
        movablePlayer.draw();
    }

    public void setMovablePlayerRigth() {
        movablePlayer.delete();
        movablePlayer = new Picture(col * BackgroundPosition.cellSize, row * BackgroundPosition.cellSize, "MovablePlayerFront.png");
        movablePlayer.draw();
    }

    public void setFightingStatusOn() {
        movablePlayerFighting = true;
    }

    public void setFightingStatusOff() {
        movablePlayerFighting = false;
    }

    public void deletePLayer() {
        movablePlayer.delete();
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KeyboardEvent.KEY_W:
                if (movablePlayerFighting) {
                    break;
                }
                setMovablePlayerUp();
                movablePlayer.translate(0, -20);
                this.row -= 2;
                break;

            case KeyboardEvent.KEY_D:
                if (movablePlayerFighting) {
                    break;
                }
                setMovablePlayerRigth();
                movablePlayer.translate(+20, 0);
                this.col += 2;
                break;

            case KeyboardEvent.KEY_A:
                if (movablePlayerFighting) {
                    break;
                }
                setMovablePlayerLeft();
                movablePlayer.translate(-20, 0);
                this.col -= 2;
                break;

            case KeyboardEvent.KEY_S:
                if (movablePlayerFighting) {
                    break;
                }
                setMovablePlayerDown();
                movablePlayer.translate(0, +20);
                this.row += 2;
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
