package org.academiadecodigo.loopeytunes.Character;

import org.academiadecodigo.loopeytunes.Game.BackgroundPosition;
import org.academiadecodigo.loopeytunes.Sound;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Fighter extends BackgroundPosition {

    protected static boolean deadFighter = false;
    protected FightOptions fightOption = FightOptions.DEFAULT;
    protected Rectangle healthBar;
    protected Rectangle healthBarBack;
    protected int healthBarEnd = 600;
    protected int healthFirstCol;
    protected int colBar;
    protected int health;
    protected int attackPower;
    protected double gamePoints = 0;
    protected int healCounter = 0;
    protected int defenseCounter = 0;
    protected boolean ultimateUsed = false;
    protected boolean defensiveStance = false;

    public Fighter(int health, int attackPower, int col, int row) {
        super(col, row);
        this.attackPower = attackPower;
        this.health = health;
        this.healthFirstCol = col * BackgroundPosition.cellSize;

    }

    public int getHealth() {
        return health;
    }

    public void healthBar() {
        healthBarBack = new Rectangle(colBar, 50, healthBarEnd, 40);
        healthBarBack.setColor(Color.RED);
        healthBarBack.fill();

        healthBar = new Rectangle(colBar, 50, healthBarEnd, 40);
        healthBar.setColor(Color.GREEN);
        healthBar.fill();
    }

    public void hit(Fighter fighter) {
        fighter.health -= attackPower;
        fighter.drawHealthBar(attackPower);
    }

    public void ultimateAttack(Fighter fighter) {
        ultimateUsed = true;
        fighter.health -= attackPower * 2;
        fighter.drawHealthBar(attackPower * 2);
    }

    public boolean defenseDepleted() {
        return defenseCounter >= 3;
    }

    public void drawHealthBar(int damage) {

        healthFirstCol += damage;
        colBar += damage * 6;
        healthBarEnd -= damage * 6;

        if (this instanceof Boss) {

            healthBar.delete();
            healthBar = new Rectangle(colBar, 50, healthBarEnd, 40);
            healthBar.setColor(Color.GREEN);
            healthBar.fill();
            return;

        }

        healthBar.delete();
        healthBar = new Rectangle(100, 50, healthBarEnd, 40);
        healthBar.setColor(Color.GREEN);
        healthBar.fill();

    }

    public boolean healDepleted() {
        return healCounter >= 3 || getHealth() >= 100;
    }

    public void heal() throws InterruptedException {
        Sound healsound = new Sound("/heal.wav");
        healsound.play(true);
        Thread.sleep(300);
        int healAmount = getRandom(15, 5);
        health += healAmount;
        if (health > 100) {
            health = 100;
            healthBar = new Rectangle(100, 50, 600, 40);
            healthBar.fill();
            healthBar.setColor(Color.GREEN);
            healCounter++;
            System.out.println("Healed to " + health);
            return;
        }
        healthBarEnd += healAmount * 6;
        colBar += healAmount * 6;
        healthBar.delete();
        healthBar = new Rectangle(100, 50, healthBarEnd, 40);
        healthBar.setColor(Color.GREEN);
        healthBar.fill();
        healCounter++;
        System.out.println("Healed to " + health);
    }

    public boolean ultimatePoints() {
        return gamePoints >= 5;
    }

    protected int getRandom(int max, int min) {
        return (int) Math.floor(Math.random() * (max - min) + min);
    }

    protected double getRandom() {
        return Math.random();
    }

    enum FightOptions {
        ATTACK,
        DEFEND,
        HEAL,
        ULTIMATE,
        DEFAULT;

    }

    public void deleteHealthBar() {
        healthBar.delete();
        healthBarBack.delete();
    }
}



