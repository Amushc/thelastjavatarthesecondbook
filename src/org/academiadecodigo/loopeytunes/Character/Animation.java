package org.academiadecodigo.loopeytunes.Character;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Animation {

    static Picture attackBoss = new Picture(1100, 330,"attackAnim.png");
    static Picture attackPlayer = new Picture(200,350,"attackAnim2.png");
    static Picture healBoss = new Picture(950,300,"healAnimOne.png");
    static Picture healPlayer = new Picture(250, 400,"healAnimTwo.png");
    static Picture chargeBoss = new Picture(1150,320,"charge.png");

    static Picture playerStance = new Picture(170, 300, "PlayerStance.png");
    static Picture playerStanceTwo = new Picture(170,300, "PlayerStanceTwo.png");
    static Picture playerAttack = new Picture(170,300,"PlayerAttack.png");
    static Picture playerUlt = new Picture(170,300,"PlayerUlt.png");
    static Picture playerHeal = new Picture(170, 300,"PlayerHeal.png");
    static Picture playerDefense = new Picture(170,390, "PlayerDefense.png");

    static Picture vandoStance = new Picture(1150, 320, "VandoStance.png");
    static Picture vandoStanceTwo = new Picture(1150,320, "VandoStanceTwo.png");
    static Picture vandoAttack = new Picture(1150,320,"VandoAttack.png");
    static Picture vandoUlt = new Picture(1150,320,"VandoUltimate.png");
    static Picture vandoHeal = new Picture(1150, 320,"VandoHeal.png");

    static Picture pedroStance = new Picture(1150, 320, "PedroStance.png");
    static Picture pedroStanceTwo = new Picture(1150,320, "PedroStanceTwo.png");
    static Picture pedroAttack = new Picture(1150,320,"PedroAttack.png");
    static Picture pedroUlt = new Picture(1150,320,"PedroUlt.png");
    static Picture pedroHeal = new Picture(1150, 320,"PedroHeal.png");

    static Picture sidStance = new Picture(1150, 320, "SidStance.png");
    static Picture sidStanceTwo = new Picture(1150,320, "SidStanceTwo.png");
    static Picture sidAttack = new Picture(1150,320,"SidAttack.png");
    static Picture sidUlt = new Picture(1150,320,"SidUlt.png");
    static Picture sidHeal = new Picture(1150, 320,"SidHeal.png");

    static Picture prisStance = new Picture(1150, 320, "PrisStance.png");
    static Picture prisStanceTwo = new Picture(1150,320, "PrisStanceTwo.png");
    static Picture prisAttack = new Picture(1150,320,"PrisAttack.png");
    static Picture prisUlt = new Picture(1150,320,"PrisUlt.png");
    static Picture prisHeal = new Picture(1150, 320,"PrisHeal.png");

    public static void initalStance(int bossId) throws InterruptedException {

        switch (bossId) {
            case 1:
                sidStanceTwo.delete();
                sidStance.draw();
                playerStanceTwo.delete();
                playerStance.draw();
                Thread.sleep(350);
                sidStance.delete();
                sidStanceTwo.draw();
                playerStance.delete();
                playerStanceTwo.draw();
                Thread.sleep(350);

                break;

            case 2:
                pedroStanceTwo.delete();
                pedroStance.draw();
                playerStanceTwo.delete();
                playerStance.draw();
                Thread.sleep(350);
                pedroStance.delete();
                pedroStanceTwo.draw();
                playerStance.delete();
                playerStanceTwo.draw();
                Thread.sleep(350);
                break;

            case 3:

                vandoStanceTwo.delete();
                vandoStance.draw();
                playerStanceTwo.delete();
                playerStance.draw();
                Thread.sleep(350);
                vandoStance.delete();
                vandoStanceTwo.draw();
                playerStance.delete();
                playerStanceTwo.draw();
                Thread.sleep(350);
                break;

            case 4:
                prisStanceTwo.delete();
                prisStance.draw();
                playerStanceTwo.delete();
                playerStance.draw();
                Thread.sleep(350);
                prisStance.delete();
                prisStanceTwo.draw();
                playerStance.delete();
                playerStanceTwo.draw();
                Thread.sleep(350);

                break;
        }



    }
    /*
    *
    * Vandos animation
     */

    public static void bossCharge(int bossId) throws InterruptedException {
        switch (bossId) {
            case 1:

                sidStance.delete();
                sidStanceTwo.delete();
                sidHeal.draw();
                chargeBoss.draw();
                Thread.sleep(800);
                sidHeal.delete();
                chargeBoss.delete();


                break;

            case 2:
                pedroStanceTwo.delete();
                pedroStance.delete();
                chargeBoss.draw();
                pedroHeal.draw();
                Thread.sleep(800);
                chargeBoss.delete();
                pedroHeal.delete();
                break;

            case 3:
                vandoStanceTwo.delete();
                vandoStance.delete();
                chargeBoss.draw();
                vandoHeal.draw();
                Thread.sleep(800);
                vandoHeal.delete();
                chargeBoss.delete();
                break;

            case 4:
                prisStanceTwo.delete();
                prisStance.delete();
                chargeBoss.draw();
                prisHeal.draw();
                Thread.sleep(800);
                chargeBoss.delete();
                prisHeal.delete();

                break;
        }

    }

    public static void vandoAttack() throws InterruptedException {
        vandoStance.delete();
        vandoStanceTwo.delete();
        vandoAttack.draw();
        attackPlayer.draw();
        Thread.sleep(800);
        vandoAttack.delete();
        attackPlayer.delete();
    }

    public static void vandoUlt() throws InterruptedException {
        vandoStance.delete();
        vandoStanceTwo.delete();
        vandoUlt.draw();
        Thread.sleep(800);
        vandoUlt.delete();
    }

    public static void vandoHeal() throws InterruptedException {
        vandoStance.delete();
        vandoStanceTwo.delete();
        vandoHeal.draw();
        healBoss.draw();
        Thread.sleep(800);
        vandoHeal.delete();
        healBoss.delete();
    }

    /*
    *
    * Pedros animation
     */

    public static void pedroAttack() throws InterruptedException {
        pedroStance.delete();
        pedroStanceTwo.delete();
        pedroAttack.draw();
        attackPlayer.draw();
        Thread.sleep(800);
        pedroAttack.delete();
        attackPlayer.delete();
    }

    public static void pedroUlt() throws InterruptedException {
        pedroStance.delete();
        pedroStanceTwo.delete();
        pedroUlt.draw();
        Thread.sleep(800);
        pedroUlt.delete();
    }

    public static void pedroHeal() throws InterruptedException {
        pedroStance.delete();
        pedroStanceTwo.delete();
        pedroHeal.draw();
        healBoss.draw();
        Thread.sleep(800);
        pedroHeal.delete();
        healBoss.delete();
    }

    /*
    *
    * Sids animation
     */


    public static void sidAttack() throws InterruptedException {
        sidStance.delete();
        sidStanceTwo.delete();
        sidAttack.draw();
        attackPlayer.draw();
        Thread.sleep(800);
        sidAttack.delete();
        attackPlayer.delete();
    }

    public static void sidUlt() throws InterruptedException {
        sidStance.delete();
        sidStanceTwo.delete();
        sidUlt.draw();
        Thread.sleep(800);
        sidUlt.delete();
    }

    public static void sidHeal() throws InterruptedException {
        sidStance.delete();
        sidStanceTwo.delete();
        sidHeal.draw();
        healBoss.draw();
        Thread.sleep(800);
        sidHeal.delete();
        healBoss.delete();
    }

    /*
    *
    * Pris animation
     */

    public static void prisAttack() throws InterruptedException {
        prisStance.delete();
        prisStanceTwo.delete();
        prisAttack.draw();
        attackPlayer.draw();
        Thread.sleep(800);
        prisAttack.delete();
        attackPlayer.delete();
    }

    public static void prisUlt() throws InterruptedException {
        prisStance.delete();
        prisStanceTwo.delete();
        prisUlt.draw();
        Thread.sleep(800);
        prisUlt.delete();
    }

    public static void prisHeal() throws InterruptedException {
        prisStance.delete();
        prisStanceTwo.delete();
        prisHeal.draw();
        healBoss.draw();
        Thread.sleep(800);
        prisHeal.delete();
        healBoss.delete();
    }

    public static void playerAttack() throws InterruptedException {
        playerStance.delete();
        playerStanceTwo.delete();
        playerAttack.draw();
        attackBoss.draw();
        Thread.sleep(800);
        attackBoss.delete();
        playerAttack.delete();
    }

    public static void playerUlt() throws InterruptedException {
        playerStance.delete();
        playerStanceTwo.delete();
        playerUlt.draw();
        Thread.sleep(800);
        playerUlt.delete();
    }

    public static void playerHeal() throws InterruptedException {
        playerStance.delete();
        playerStanceTwo.delete();
        playerHeal.draw();
        healPlayer.draw();
        Thread.sleep(800);
        playerHeal.delete();
        healPlayer.delete();
    }

    public static void playerDefense() throws InterruptedException {

        playerStance.delete();
        playerStanceTwo.delete();
        playerDefense.draw();
        Thread.sleep(800);
        playerDefense.delete();

    }

    public static void deleteAnimation(){
        playerStance.delete();
        playerStanceTwo.delete();
        sidStance.delete();
        sidStanceTwo.delete();
        pedroStance.delete();
        pedroStanceTwo.delete();
        prisStance.delete();
        prisStanceTwo.delete();
        vandoStance.delete();
        vandoStanceTwo.delete();
    }
}