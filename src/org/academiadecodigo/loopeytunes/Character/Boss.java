package org.academiadecodigo.loopeytunes.Character;

import org.academiadecodigo.loopeytunes.Sound;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Boss extends Fighter {
    static double bossPowerLvl = 0.1;
    int bossUlti;

    private boolean isCharged = false;

    public Boss(int health, int attackPower, int col, int row) {
        super(health, attackPower, col, row);

        colBar = 920;
    }


    public void defeatedBossCounter() {
        bossPowerLvl += 0.1;
        bossUlti = 0;
    }

    public boolean isDead() {
        return health <= 0;
    }

    public void heal(int healthCharge) {
        Sound healsound = new Sound("/heal.wav");
        healsound.play(true);
        health += healthCharge;
        if (getHealth() > 100) {
            healthCharge -= (getHealth() - 100);
            this.health = 100;
        }
        colBar -= healthCharge * 6;
        healthBarEnd += healthCharge * 6;

        healthBar.delete();

        healthBar = new Rectangle(colBar, 50, healthBarEnd, 40);
        healthBar.setColor(Color.GREEN);
        healthBar.fill();

    }

    public void fightOption(Fighter fighter, int bossID) throws InterruptedException {

        double rand = getRandom();

        if (isCharged && !fighter.defensiveStance) {

            switch (bossID) {

                case 1:
                    Animation.sidUlt();
                    break;

                case 2:
                    Animation.pedroUlt();
                    break;

                case 3:
                    Animation.vandoUlt();
                    break;

                case 4:
                    Animation.prisUlt();
                    break;
            }

            Sound bossAtack = new Sound("/atackSound.wav");
            bossAtack.play(true);
            ultimateAttack(fighter);
            isCharged = false;
            bossUlti++;
            return;
        }

        if (rand < bossPowerLvl && healCounter <= 3) {

            switch (bossID) {

                case 1:

                    Animation.sidHeal();
                    break;

                case 2:

                    Animation.pedroHeal();
                    break;

                case 3:

                    Animation.vandoHeal();
                    break;

                case 4:

                    Animation.prisHeal();
                    break;
            }

            heal(getRandom(15, 5));
            Sound healSound = new Sound("/heal.wav");
            healSound.play(true);
            healCounter++;
            return;
        }

        // BOSS ULTI
        if (fighter.defensiveStance) {
            isCharged = false;
            return;
        }


        if (rand > 1 - bossPowerLvl && bossUlti <= 2) {

            isCharged = true;

            Animation.bossCharge(bossID);

        } else {

            Sound bossAtack = new Sound("/atackSound.wav");
            bossAtack.play(true);
            hit(fighter);

            switch (bossID) {
                case 1:
                    Animation.sidAttack();
                    break;

                case 2:
                    Animation.pedroAttack();
                    break;

                case 3:
                    Animation.vandoAttack();
                    break;

                case 4:
                    Animation.prisAttack();
                    break;
            }

            bossAtack.stop();
        }
    }

}
