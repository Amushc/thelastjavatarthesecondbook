package org.academiadecodigo.loopeytunes.Character;

import org.academiadecodigo.loopeytunes.Sound;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Player extends Fighter implements KeyboardHandler {

    private boolean ultimateUsed = false;

    public Player(int health, int attackPower, int col, int row) {
        super(health, attackPower, col, row);
        colBar = 100;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_1) {
            fightOption = FightOptions.ATTACK;
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_2) {
            fightOption = FightOptions.DEFEND;
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_3) {
            fightOption = FightOptions.HEAL;
        }
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_4) {
            fightOption = FightOptions.ULTIMATE;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public void chooseOption(Boss boss , int bossID) throws InterruptedException {
        Sound soundAnimation;
        defensiveStance = false;

        switch (fightOption) {

            case ATTACK:
                soundAnimation = new Sound("/atackSound.wav");
                soundAnimation.play(true);
                Thread.sleep(300);
                hit(boss);
                gamePoints++;
                Animation.playerAttack();
                Animation.initalStance(bossID);
                fightOption = FightOptions.DEFAULT;
                if(boss.isDead()) {break;}
                boss.fightOption(this , bossID);
                break;

            case DEFEND:
                soundAnimation = new Sound("/heal.wav");
                soundAnimation.play(true);
                Animation.playerDefense();
                Animation.initalStance(bossID);
                if (defenseDepleted()) {
                    fightOption = FightOptions.DEFAULT;
                    break;
                }
                defensiveStance = true;
                defenseCounter++;
                fightOption = FightOptions.DEFAULT;
                boss.fightOption(this , bossID);
                break;

            case HEAL:
                Thread.sleep(200);
                if (healDepleted()) {
                    fightOption = FightOptions.DEFAULT;
                    break;
                }
                heal();
                Animation.playerHeal();
                Animation.initalStance(bossID);
                fightOption = FightOptions.DEFAULT;
                healCounter++;
                boss.fightOption(this , bossID);
                break;

            case ULTIMATE:
                soundAnimation = new Sound("/atackSound.wav");
                soundAnimation.play(true);
                if (!ultimatePoints()) {
                    fightOption = FightOptions.DEFAULT;
                    break;
                }
                if ((!ultimateUsed)) {
                    Animation.playerUlt();
                    Animation.initalStance(bossID);
                    ultimateAttack(boss);

                }

                fightOption = FightOptions.DEFAULT;
                if(boss.isDead()) {break;}
                boss.fightOption(this , bossID);
                break;

            default:
                soundAnimation = new Sound("/atackSound.wav");
                Thread.sleep(1);

                if (this.getHealth() <= 0 || boss.getHealth() <= 0) {
                    deadFighter = true;
                    break;
                }
                soundAnimation.stop();
                break;
        }
    }
}
